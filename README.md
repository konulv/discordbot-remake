run:
bash/linux:
source venv/bin/activate  
python -3.8 -m venv "\<folders path>"  
ctr + k, ctr + 0 fold all  or ctrl + shift + -
ctr + k, ctr + j unfold  
`pip install -r requirements.txt`  
  
to run bot, create a new file named `.env` at same place as `secrets.env` file, with same content, except value holders replaced with actual values. `.env` been added to .gitignore so it shouldn't be picked up when updating/pulling
  
requirements: 
check requirements.txt file, or can also be passed into pip for auto install  
Note: youtube_dl uses outdated parameters, like "dislike count" that has been removed, dont forget to go into the "back" to comment out line 54 in backend_youtube_dl.py that tries to access such parameter  
  
ffmpeg needs to be installed to work
Coverage.py Highlighter - plugin is helpful

python -m unittest tests/*
python -m unittest discover

coverage run -m unittest discover
coverage json

coverage report -m
