from discord.ext.commands import Cog, command, Bot
import random

class Template(Cog):
    def __init__(self, bot):
        self.bot = bot

    @command(name="template",
                 description="it does nothing",
                 brief="showcase how to do cogs",
                 aliases=["useless"])
    async def template(self, ctx):
        await ctx.send("I'm not sure how you managed to invoke this command, but have a cookie, it does nothing")



async def setup(bot):
    await bot.add_cog(Template(bot))

