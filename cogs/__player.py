from youtubesearchpython.__future__ import VideosSearch
from discord import VoiceChannel, FFmpegPCMAudio, ui, SelectOption, Interaction
from pafy import new
import asyncio

FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5',
                'options': '-vn'}
YOUTUBE_LINK = "https://www.youtube.com/watch?v="

class Player:
    def __init__(self, player, ctx):
        self.queue = []
        self.player = player
        self.ctx = ctx
        self.current = ""
        
    async def search(self, text, num=5):
        print(f"search: {text}\n{num}")
        srch = VideosSearch(text, limit=num)
        a = await srch.next()
        a = a["result"]
        if num==1: #i.e this gets called by "play"
            return a[0]

        view = ui.View()
        view.add_item(Dropdown(a))
        await self.ctx.send('Search for your song:', view=view)
        await view.wait()

        song = view.children[0].choice
        if song != -1:
            await self.play(a[song], True)
            return

        await self.ctx.send("selection has timed out")
       
        
    
    async def play(self, text, found=False, time=0):
        print(f"play: {text}\n{found}\n{time}")
        if not found:
            audio_details = await self.search(text, 1)
        else:
            audio_details = text
        # else: #wtf does this do?
        #     audio = text
        # questionable variable overwriting above

        if self.player.is_playing():
            self.que(audio_details, time)
            await self.ctx.send(f"quing up {audio_details['title']}")
            return
        else:
            await self.ctx.send(f"now playing {audio_details['title']}")
            await self.__play_song(audio_details)
            await asyncio.sleep(time)
            
            

        while bool(self.queue):
            while self.player.is_playing():
                await asyncio.sleep(1)
            next = self.deque()
            await self.ctx.send(f"now playing {next[0]['title']}")
            await self.__play_song(next[0], next[1]) # send text before playing
            await asyncio.sleep(next[1])
            

    async def __play_song(self, song_details, time=0):
        print(f"__play: {song_details}\n{time}")
        audio_details = song_details
        if time == 0:
            time = audio_details["duration"].split(":")
            time.reverse()
            time = sum([int(time[i])*(60**i) for i in range(len(time))])
        audio = audio_details["id"]

        audio = new(audio, basic=False).getbestaudio()
        print(audio.url)
        source = FFmpegPCMAudio(audio.url, **FFMPEG_OPTIONS)
        self.player.play(source)
        self.current = [audio_details, time]
        await asyncio.sleep(time)

    async def skip(self):
        print("skip")
        if not bool(self.queue): # i.e. empty
            await self.stop()
        else:
            next = self.deque()
            await self.ctx.send(f"now playing {next[0]['title']}")
            self.player.stop()

            await self.__play_song(next[0], next[1]) # send text before playing
            await asyncio.sleep(next[1])
        
    async def stop(self):
        print("stop")
        self.player.stop()
        await self.ctx.send("stopping")

    async def resume(self):
        print("resume")
        if not self.player.is_playing():
            self.player.resume()



    def show_queue(self):
        print("show queue")
        np = [f"{1}: {self.current[0]['title']} {self.current[0]['duration']}"]
        queue = [f"{index+2}: {val[0]['title']} {val[0]['duration']}" for index, val in enumerate(self.queue)]
        return np + queue

    def remove(self,num):
        pass

    async def refresh(self): # restart at current song
        print("refresh")
        self.player.stop()
        await self.ctx.send(f"refreshing current song: {self.current[0]['title']}")
        await self.__play_song(self.current[0], self.current[1])

    def current_song(self):
        print("current song")
        np = f"{self.current[0]['title']} {self.current[0]['duration']}"
        return np
    
    def que(self, audio_details, time=0):
        print("que")
        self.queue.append([audio_details, time])

    def deque(self):
        print("deque")
        audio_time = self.queue[0]
        del self.queue[0]
        return audio_time

    def move(self, move_from, move_to):
        print("move")
        pass
    

class Dropdown(ui.Select):
    def __init__(self, song_list):
        self.songs = [i["title"] for i in song_list]
        options = []
        for i in song_list:
            options.append(SelectOption(label=i["title"], description=f'duration: {i["duration"]}'))
        super().__init__(placeholder='Top 10 search results...', min_values=1, max_values=1, options=options)

    async def callback(self, interaction: Interaction):
        self.disabled = True
        self.view.stop()
        self.choice = self.songs.index(self.values[0])
        # await self.view.stop()
        await interaction.response.send_message(f'you have selected: {self.values[0]}')
        self.view.stop()