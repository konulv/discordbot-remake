# helpful stack overflow 
# https://stackoverflow.com/questions/49354232/how-to-stream-audio-from-a-youtube-url-in-python-without-download
# stack 2:
# https://stackoverflow.com/questions/66115216/discord-py-play-audio-from-url

from distutils.command.build import build
from pydoc import describe
from webbrowser import get
from discord.ext.commands import Cog, command, Bot
from discord import VoiceChannel, FFmpegPCMAudio
from multidict import getversion
from os import getenv
from aiohttp import ClientSession
from json import loads
import random
from .__player import Player

YOUTUBE_LINK = "https://www.youtube.com/watch?v="

# got no idea what these options are tbh, just appropriating from stack2
FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5',
                'options': '-vn'}



class Music(Cog):
    def __init__(self, bot):
        self.bot = bot

    @command(name="play",
             aliases=["p","pl"],
             brief="fetches music from youtube to play",
             description="the command takes argument of any search term")
    async def play(self, ctx, *, arg):
        joined = self.__check_joined(ctx)
        if not joined:
            await self.join(ctx, False)


        guild = ctx.guild.id
        channel = self.bot.voiceChats[guild]
        player = channel[0] # this needs better naming
        await player.play(arg)
         

    @command(name="search",
            aliases=["s"],
            brief="searches for music in youtube",
            description="the command takes argument of any search term")
    async def search(self, ctx, *, arg):
        joined = self.__check_joined(ctx)
        if not joined:
            await self.join(ctx, False)

        guild = ctx.guild.id
        channel = self.bot.voiceChats[guild]
        player = channel[0] # this needs better naming
        await player.search(arg, 10)


    @command(name="skip")
    async def skip(self, ctx):
        guild = ctx.guild.id
        player = self.bot.voiceChats[guild][0]
        await player.skip()

    @command(name="stop")
    async def stop(self, ctx):
        guild = ctx.guild.id
        player = self.bot.voiceChats[guild][0]
        await player.stop()

    @command(name="resume")
    async def resume(self, ctx):
        guild = ctx.guild.id
        player = self.bot.voiceChats[guild][0]
        await player.resume()

    @command(name="join")
    async def join(self, ctx, display=True):
        channel = ctx.author.voice
        guild = ctx.guild.id

        if channel is None:
            await ctx.send("you are not in a voice channel :(")
            return

        joined = self.__check_joined(ctx)
        if not joined:
            
            self.bot.voiceChats[guild] = [Player(await channel.channel.connect(), ctx)]
            return
        else:
            chat = self.bot.voiceChats[guild]
            if display:
                if chat[0].player.channel.id == channel.channel.id:
                    await ctx.send("But I'm already here!")
                else:
                    await ctx.send("Sorry, i can only join 1 channel :(")

    # this probs needs some checking cause of possability for trolling
    # as making it leave can be done by anyone, from anywhere
    @command(name="leave")
    async def leave(self, ctx):
        guild = ctx.guild.id

        try:
            channel = self.bot.voiceChats[guild]
            if channel is not None:
                await channel[0].player.disconnect()
                self.bot.voiceChats[guild] = []
                return
        except KeyError:
            pass

        await ctx.send("B-but I'm not in a voice channel")

    # return True if it is in a channel already, false otherwise
    def __check_joined(self, ctx):
        guild = ctx.guild.id
        channel = "" #needs better naming

        try:
            channel = self.bot.voiceChats[guild] # this might error if bot hasn't joined that server before
            if not bool(channel): # if bot is not in a channel, channel will be []
                return False
            else: 
                return True
        except KeyError: # if its first time the bot is seeing the guild, it will error, hence it hasn't joined
            return False


    @command(name="nowplaying",
             aliases=["np"],
             brief="shows current song")
    async def now_playing(self, ctx):
        guild = ctx.guild.id
        player = self.bot.voiceChats[guild][0]
        np = player.current_song()
        await ctx.send(np)

    @command(name="queue",
             aliases=["q","que"],
             brief="shows current queue")
    async def queue(self, ctx):
        guild = ctx.guild.id
        player = self.bot.voiceChats[guild][0]
        queue = player.show_queue()
        string = f"{queue[0]}\n-------------\n"
        string += "\n".join(queue[1:])
        await ctx.send(string)

    @command(name="refresh",
             aliases=["r","rr"],
             brief="in an odd case bot isn't playing, use this")
    async def refresh(self, ctx):
        guild = ctx.guild.id
        player = self.bot.voiceChats[guild][0]
        await player.refresh()



async def setup(bot):
    await bot.add_cog(Music(bot))

