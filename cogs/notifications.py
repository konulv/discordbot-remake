from discord.ext.commands import Cog, command, Bot
import database.db as db

class Notifications(Cog):
    def __init__(self, bot):
        db.connect("database/database.db")
        self.bot = bot

    @command(name="test",
                 description="it does nothing",
                 brief="showcase how to do cogs",
                 aliases=["useless"])
    async def test(self, ctx):
        db._hello()
        await ctx.send("I'm not sure how you managed to invoke this command, but have a cookie, it does nothing")
    

    @command(name="SetConfig", aliases=["setconfig", "setConfig"])
    async def setConfig(self, ctx,  *, args=""):
        if args == "": 
            await ctx.send("you forgot to add any arguments, silly. Need reminder and time zone")
            return
        args = args.split(" ")
        if len(args) < 2:
            await ctx.send("too few arguments given, 2 needed: reminder and time zone")
        elif len(args) > 2:
            await ctx.send("too many arguments given, 2 needed: reminder and time zone")
        else:
            channel = ctx.channel.id
            await ctx.send("attemtping to set up config")
            try:
                db.addConfig(channel, args[0], args[1])
            except db.ConfigError as e:
                await ctx.send(str(e))
                return

            await ctx.send("check the db to see if it worked")

    @command(name="updateConfig",
                 description="update config",
                 brief="updateConfig",
                 aliases=["Updateconfig", "UpdateConfig", "updateconfig"])
    async def updateConfig(self, ctx, *, args=""):
        if args == "": 
            await ctx.send("you forgot to add any arguments, silly. Need reminder and time zone")
            return

        args = args.split(" ")
        if len(args) < 2:
            await ctx.send("too few arguments given, 2 needed: reminder and time zone")
        elif len(args) > 2:
            await ctx.send("too many arguments given, 2 needed: reminder and time zone")
        else:
            channel = ctx.channel.id
            await ctx.send("attemtping to get config")
            
            try:
                conf = db.getConfig(channel)
            except db.ConfigError as e:
                await ctx.send(str(e))
                return
            
            if conf is None:
                await ctx.send("it appears that this channel doesn't have config set up yet")
                
            print(conf)
            try:
                db.updateConfig(conf[0], args[0], args[1])
            except db.ConfigError as e:
                await ctx.send(str(e))
                return

            await ctx.send("check the db to see if it worked")




async def setup(bot):
    await bot.add_cog(Notifications(bot))

