import unittest

from database import db
from os import system as cmd

class TestDb(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        db.connect("tests/testdb.db")
        
        db.createTable()
        db.addConfig("321", 0, "utc") #shouldn't change
        db.addConfig("456", 0, "utc") #updateConfig changes this
        db.addConfig("789", 0, "utc") #to be deleted by configDelete
        db.addConfig("1234", 0, "utc") #space

        db.addTimer("321", "2022-01-15", "12:30")
        db.addTimer("321", "2022-01-16", "20:31")
        db.addTimer("456", "2023-01-11", "20:30")
        db.addTimer("1234", "2023-06-01", "02:30")

    @classmethod
    def tearDownClass(cls):
        # pass
        cmd("rm tests/testdb.db")


    #done
    def test_addConfig(self):
        #perfectly fine
        self.assertTrue(db.addConfig("123", 0, "utc"))

        #repeated entry
        with self.assertRaises(db.ConfigError):
            db.addConfig("123", 0, "utc")

        #invalid reminder
        with self.assertRaises(db.ConfigError):
            db.addConfig("124", "abc", "utc")

        #invalid timezone
        with self.assertRaises(db.ConfigError):
            db.addConfig("125", 0, "usb")

        #negative reminder
        with self.assertRaises(db.ConfigError):
            db.addConfig("126", -1, "utc")

        # invalid reminder and time zone
        with self.assertRaises(db.ConfigError):
            db.addConfig("127", -1, "usb")
            
    #done
    def test_getConfig(self):
        #getting by channel id
        self.assertEqual(db.getConfig(321), (1, 321, 0, "utc"))
        
        #getting by configID
        self.assertEqual(db.getConfig(1), (1, 321, 0, "utc"))

        #doesn't exist, returns none
        self.assertEqual(db.getConfig(9999), None)

    ##done?
    def test_updateConfig(self):
        db.updateConfig(2, 5, "utc")
        self.assertEqual(db.getConfig(2), (2, 456, 5, "utc"))

        #wrong time zone
        with self.assertRaises(db.ConfigError):
            db.updateConfig(2, 5, "usb")

        #negative reminder
        with self.assertRaises(db.ConfigError):
            db.updateConfig(2, -5, "utc")

        #non existing config
        with self.assertRaises(db.ConfigError):
            db.updateConfig(99999, 5, "utc")

        #double checking that nothing changed as above errors should been aborted
        self.assertEqual(db.getConfig(2), (2, 456, 5, "utc"))
        
        #doesn't touch other areas 
        self.assertEqual(db.getConfig(1), (1, 321, 0, "utc"))
        self.assertEqual(db.getConfig(4), (4, 1234, 0, "utc"))

    ##fails
    def test_removeConfig(self):
        pass
        # self.assertTrue(db.removeConfig(3)) # successful removal

        # self.assertEqual(db.getConfig(3), None) # checking db to be sure

        # with self.assertRaises(db.ConfigError):
        #     db.removeConfig(99999) # config doesn't exist

    ##done?
    def test_addTimer(self):
        db.addConfig("222", 5, "utc")
        # db.addTimer(channelID, date, time, message="Only Cthulhu knows, try asking him", repeatMode=0)
        self.assertTrue(db.addTimer("222", "2023-01-15", "12:30"))

        #non existing channel/config
        with self.assertRaises(db.ConfigError):
            db.addTimer("333", "2023-01-15", "12:30")

        # bad date
        with self.assertRaises(db.ConfigError):
            db.addTimer("222", "kek", "12:30")

        # bad date 2
        with self.assertRaises(db.ConfigError):
            db.addTimer("222", "2023-13-40", "12:30")

        # bad time
        with self.assertRaises(db.ConfigError):
            db.addTimer("222", "2023-01-15", "i like trains")

        # bad time 2
        with self.assertRaises(db.ConfigError):
            db.addTimer("222", "2023-01-15", "25:67")
        
    def test_updateTimer(self):
        pass

    def test_getChannelEvent(self):
        pass
    
    #done
    def test_getTimer(self):
        self.assertEqual(db.getTimer(1,321), ('2022-01-15 12:30', '0', 'Only Cthulhu knows, try asking him'))
        self.assertEqual(db.getTimer(2,321), ('2022-01-16 20:31', "0", 'Only Cthulhu knows, try asking him'))
        self.assertEqual(db.getTimer(3,456), ('2023-01-11 20:30', "0", 'Only Cthulhu knows, try asking him'))
        self.assertEqual(db.getTimer(4,1234), ('2023-06-01 02:30', "0", 'Only Cthulhu knows, try asking him'))
        self.assertEqual(db.getTimer(25, 0), None)



if __name__ == "__main__":
    print("hi")
    unittest.main()