from datetime import time
import pytz

ALL_TIME_ZONES = [x.lower() for x in pytz.common_timezones]
reminder_limit = 60

class Config:

    def __init__(self, channelID, reminder, timeZone, configID=0):
        
        
        errors = ""
        try:
            temp = int(channelID)
            temp = int(configID)
        except ValueError:
            raise ConfigError("Channel and/or Config IDs couldn't be picked " +
                            "or been forcebly incorrectly entered. Please contact" +
                            " the dev as this is a major break in the system"+
                            " of how this was achieved")
        
        try:
            temp = int(reminder)
            if temp > 60:
                errors += f"Reminder cant be bigger then {reminder_limit}min"
        except ValueError:
            errors += f"reminder '{reminder}' must be a number"

        if not(timeZone.lower() in ALL_TIME_ZONES):
            errors += "\n" if errors else "" # sorting out the formatting
            errors += f"TimeZone '{timeZone}' not found"
        else:
            # in case time zone is valid but cases are wrong, e.g. UtC but i need UTC
            timeZone = pytz.common_timezones[ALL_TIME_ZONES.index(timeZone.lower())]

        if errors: # will spit an error if string is non-empty
            raise ConfigError(errors)

        self.__channelID = channelID
        self.__reminder = int(reminder)
        self.__timeZone = timeZone
        self.__configID = configID

    def editReminder(self, reminder):
        try:
            temp = int(reminder)
            if temp > 60:
                raise ConfigError(f"Reminder cant be bigger then {reminder_limit}min")
        except ValueError:
            raise ConfigError(f"reminder '{reminder}' must be a number")
        
        self.__reminder = reminder

    def editTimeZone(self, timeZone):
        if not(timeZone.lower() in ALL_TIME_ZONES):
            raise ConfigError(f"TimeZone '{timeZone}' not found")
        else:
            # in case time zone is valid but cases are wrong, e.g. UtC but i need UTC
            timeZone = pytz.common_timezones[ALL_TIME_ZONES.index(timeZone.lower())]

        self.__timeZone = timeZone

    def __repr__(self):
        return f"ConfigID: {self.__configID}\nTimeZone: {self.__timeZone}\nReminder: {self.__reminder}min\nChannelID: {self.__channelID}"

    @property
    def reminder(self):
        return self.__reminder

    @property
    def timeZone(self):
        return self.__timeZone

    @property
    def channelID(self):
        return self.__channelID

    # TODO this probably needs to be connected to the db to fetch correct ID
    # also, a question, is this even needed?
    @property
    def configID(self):
        return self.__configID
class ConfigError(Exception):
    pass
