Repeat modes:
0 - no repeats
1 - daily repeats
2 - weekly repeats
3 - monthly repeats

hear me out:
#d
#w
#m
#y
like repeat mode being 9d - 9 days
2m - every 2 months
2d1m every every month and 2 days (e.g. 1st sep, 3rd oct, 5th nov, etc)
that means i would need an interpreter for this bs

Config:
 channel ID = on which channel notifications will be sent on
 reminder = time in minutes before the event happens, sends out a reminder
 timezone = time zone, to account for different servers working on different clocks

    note: config will need a process of setting up, potential tutorial/guidance event

Timer:
configID = configID
Date = date on which event will happen
RepeatMode = check above
message = message for event

