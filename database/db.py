import sqlite3
import pytz
from datetime import datetime, timedelta
from typing import Union
#from event import Event

ALL_TIME_ZONES = [x.lower() for x in pytz.common_timezones]

conn = ""
c = ""

def connect(path: str):
    global conn
    global c

    conn = sqlite3.connect(path)
    c = conn.cursor()


## delete later
# c.execute("insert into config(ChannelID, Reminder, TimeZone) values (123132, 20, 'UTC')")
# conn.commit()
#
# print(c.execute("SELECT * FROM config WHERE ConfigID == 1").fetchone())

# done, tested, unit tested
def addConfig(channelID: Union[str , int], reminder: str, timeZone: str) -> bool:

    check = c.execute("SELECT * FROM config WHERE  ChannelID == :ID", {"ID": channelID}).fetchall()

    if check:
        raise ConfigError("This channel already has config set up")

    __verifyConfig(reminder, timeZone) # can potentially error, let it bubble

    c.execute("INSERT into config(ChannelID, Reminder, TimeZone) values (:channelID, :reminder, :timeZone)",
              {"channelID": channelID, "reminder": reminder, "timeZone": timeZone})
    conn.commit()

    return True


# get config either by providing config ID or channel ID
# returns None if there is no config
# done, untested, unit tested
def getConfig(ID: Union[str, int]):
    data = c.execute("SELECT * FROM config WHERE (:ID) == ConfigID", {"ID": ID}).fetchone()
    if data is None:
        data = c.execute("SELECT * FROM config WHERE (:ID) == ChannelID", {"ID": ID}).fetchone()
    return data
    

# done, untested, unverified inputs, unit tested
def updateConfig(ConfigID: Union[str, int], reminder: str, timeZone: str):
    check = c.execute("SELECT * FROM config WHERE  ConfigID == :ID", {"ID": ConfigID}).fetchall()
    if not check:
        raise ConfigError("No Config for the given ID")
    
    
    __verifyConfig(reminder, timeZone)

    # print(ConfigID, type(ConfigID), reminder, type(reminder), timeZone, type(timeZone))
    c.execute("UPDATE config SET Reminder = (:reminder), TimeZone = (:timeZone) WHERE ConfigID = (:ID)", 
              {"reminder": reminder, "timeZone": timeZone, "ID": ConfigID})
    conn.commit()
    #will configError

def __verifyConfig(reminder: str, timeZone: str):
    errors = ""
    try:
        temp = int(reminder)
        if temp < 0:
            errors += f"you've entered a negative value for reminder\n"
    except ValueError:
        errors += f"reminder '{reminder}' must be a number\n"

    if not(timeZone.lower() in ALL_TIME_ZONES):
        errors += f"TimeZone '{timeZone}' not found"
    else:
        # in case time zone is valid but cases are wrong, e.g. UtC but i need UTC
        # TODO wtf is this?
        timeZone = pytz.common_timezones[ALL_TIME_ZONES.index(timeZone.lower())]

    if errors:
        raise ConfigError(errors)

#TODO, not prio, unit tested
def removeConfig(ConfigID: Union[str, int]):
    pass

#done, untested
def addTimer(channelID: Union[str, int], date: str, time: str, message="Only Cthulhu knows, try asking him", repeatMode="0"):

    errors = ""
    # validations
    try:
        configID = getConfig(channelID)[0]
    except TypeError:
        raise ConfigError("Config hasn't been set up on this channel")

    try:
        datetime.strptime(date, "%Y-%m-%d")
    except ValueError:
        errors += f"Date '{date}' is invalid, check if its in a correct format YYYY-MM-DD\n"

    try:
        datetime.strptime(time, "%H:%M")
    except ValueError:
        errors += f"Date '{time}' is invalid, check if its in a correct format HH:MM"


    if errors:
        raise ConfigError(errors)
    
    dateTime = date + " " + time
    # doing

    c.execute("INSERT into timer(ConfigID, DateTime, RepeatMode, Message) "
              "values (:ConfigID, :Datetime, :RepeatMode, :Message)",
              {"ConfigID": configID, "Datetime": dateTime,
                "RepeatMode": repeatMode, "Message": message})
    conn.commit()
    return True


#TODO, not prio
def updateTimer(channelID: Union[str, int], date: str, time: str, message: str, repeatMode: str):
    pass


#TODO, not prio
def getChannelEvents(ID: Union[str, int]):
    try:
        config = getConfig(ID)[0]
    except TypeError:
        raise EventError("Config hasn't been set up on this channel")

    data = c.execute("SELECT * FROM event, config WHERE event.ConfigID == config.ConfigID AND (:config) == event.ConfigID",
                     {"config": config}).fetchall()
    if not data:
        data = None
    # TODO to be consistent or not to be. To return a list of events or not.
    return data


def _hello():
    print("hello world")

# Do i even need this? yes
# hasn't been checked to be working with current stuff
#TODO replanning needed, this got named from event to timer
def getTimer(ID, channelID):
    data = c.execute("SELECT DateTime, RepeatMode, Message "
                     "FROM timer, config "
                     "WHERE config.ConfigID == timer.ConfigID "
                     "AND (:channelID) == config.ChannelID "
                     "AND (:ID) == TimerID", 
                     {"ID": ID, "channelID": channelID}).fetchone()
    if not data: # if empty
        return None
    
    return data

## works by providing either channelID or config ID
## return None if nothing
def getAllTimers(ID: Union[str, int]):
    data = c.execute("SELECT TimerID, DateTime, RepeatMode, Message "
                     "FROM timer, config "
                     "WHERE config.ConfigID == timer.ConfigID "
                     "AND (:ID) == config.ChannelID ", 
                     {"ID": ID}).fetchall()
    
    if not data: #if empty
        return None
    
    return data
    



class ConfigError(Exception):
    pass

class EventError(Exception):
    pass


def createTable():
    c.execute("""CREATE TABLE config (
            ConfigID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            ChannelID INTEGER NOT NULL,
            Reminder INTEGER NOT NULL,
            TimeZone TEXT NOT NULL)""")
    #does sqlite not handle date time?
    # dude what is this? DateTime is an Integer but im yeeting strings into it? and sqlite just accepts that?
    c.execute("""CREATE TABLE timer (
            TimerID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            ConfigID INTEGER NOT NULL,
            DateTime INTEGER NOT NULL,
            RepeatMode TEXT NOT NULL,
            Message TEXT NOT NULL,
            FOREIGN KEY(ConfigID) REFERENCES config(ConfigID))""")

	
if __name__ == "__main__":
    
    connect("database/database.db")
    createTable()