from datetime import datetime, timedelta

class Timer:

    def __init__(self, date, time, repeatMode, message, configID=0, timerID=0):
        fulldate = date + " " + time
        date = datetime.strptime(fulldate, "%d/%m/%Y %H:%M")
        self.__date = date
        self.__time = time,
        self.__repeatMode = repeatMode
        self.__message = message
        self.__configID = configID
        self.__timerID = timerID

    def get_next(self):
        return str(self.__date)
    
    def apply_repeatMode(self):
        if self.__repeatMode == "0":
            return None
        
