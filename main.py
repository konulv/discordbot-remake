# live bot: https://discordapp.com/oauth2/authorize?client_id=506514227276808192&scope=bot
# test bot: https://discordapp.com/oauth2/authorize?client_id=519864297656942602&scope=bot
# discord documentation https://tinyurl.com/y9nuae8t
# https://discordpy.readthedocs.io/en/master/

from ast import arg
from sqlite3 import InternalError
from dotenv import load_dotenv
from os import getenv
from sys import argv
import asyncio
from aiohttp import web


from datetime import datetime
from discord import Game, Embed, Intents
from discord.ext.commands import Bot, Cog

#defining intents
intents = Intents.default()
intents.bans = False
intents.guild_typing = False
intents.invites = False
intents.typing = False
intents.message_content = True


load_dotenv()
LIVE_TOKEN = getenv("LIVE_BOT_KEY")
TEST_TOKEN = getenv("TEST_BOT_KEY")

if LIVE_TOKEN is None:
    print("You haven't set up .env file correctly, as no token has been found")
    exit()

BOT_PREFIX = ("?")
bot = Bot(command_prefix=BOT_PREFIX, intents=intents)

bot.voiceChats = {} # used as {guildID: [player, Channel]}


@bot.command(name="whereYaGoing")
async def emoji1(ctx):
    img = Embed().set_image(url="http://i.imgur.com/fVDH5bN.gif")
    await ctx.send(embed=img)


@bot.command(name="git")
async def git(ctx):
    img = Embed(title="Git pages:", description="Bot's: https://gitlab.com/konulv/discordbot-remake\n"
    + "Devs's: https://gitlab.com/konulv")
    await ctx.send(embed=img) 


## look into aiohttp for interacting with APIs

# @bot.command()
# async def bitcoin(ctx):
#     url = "https://api.coindesk.com/v1/bpi/currentprice/BTC.json"
#     response = web.get(url, handle)
#     value = response.json()["bpi"]["USD"]["rate"]
#     await ctx.send("Bitcoin price is: $" + value)


# @bot.command(name="spam",
#              brief="sets status for spamming",
#              description='do "True" if ya want to spam, "False" otherwise.')
# async def spam(ctx):
#     content = ctx.message.content[6:].lower()
#     boolean = content == "true"
#     set_spam_status(boolean)
#     await ctx.send("spamming been stopped" if not (get_spam_status()) else "spam continues")


# @bot.command(name="spamChat",
#              brief="rn just spams chat every 30s",
#              description="proof of concept, completely useless currently")
# async def spam_chat(ctx):
#     while get_spam_status():
#         time = datetime.today()
#         if time.second % 5 == 0:
#             await ctx.send("@Konulv#5775")
#         await asyncio.sleep(1)


@bot.event
async def on_ready():
    game = Game("Type ?help for cmds")
    await bot.change_presence(activity=game)
    print("Logged in as " + bot.user.name)


async def main(TOKEN):
    async with bot:
        print("loading general cog")
        await bot.load_extension("cogs.general")

        # print("loading music cog")
        # await bot.load_extension("cogs.music")

        print("loading notifications cog")
        await bot.load_extension("cogs.notifications")
        
        print("starting bot")
        await bot.start(TOKEN)
        

# bot.load_extension("cogs.general")
# bot.load_extension("cogs.music")

if(__name__=="__main__"):
    try:
        if argv[1] == "-dev":
            asyncio.run(main(TEST_TOKEN))
        elif argv[1] == "-live":
            asyncio.run(main(LIVE_TOKEN))
        else:
            print("wrong flag given\navailable flags:\n"+
            "-dev for testrun\n-live for deploying")
    except IndexError:
        print("you forgot to give a flag for running this bot\nflags available:"+
        "\n-dev for test bot\n-live for deploying")